import sys
import json
import requests

WORKSPACE = sys.argv[4]
with open(str(WORKSPACE)+'/token.json') as f:
    data = json.load(f)

token = data['token']

with open(str(WORKSPACE) +'/extracted_userData.json') as f:
    data = json.load(f)
MainframeIP = data["MainframeIP"]
Username = data["username"]

ARTIFACT_VERSION = sys.argv[1]
MY_URL = sys.argv[2]
MODEL_VERSION = sys.argv[3]
SCORING_ID = sys.argv[5]

header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
            'ML-Instance-Id' : Username
}
print(ARTIFACT_VERSION)

DEPLOYMENT_NAME = "Canary-Deploy-PRODv" + str(MODEL_VERSION)

response = requests.get("https://" + MainframeIP + ":11442/v3/deployments", headers=header,verify=False)
data = json.loads(response.text)

target_model_name = DEPLOYMENT_NAME

Index = None

for index, resource in enumerate(data["resources"]):
    model_name_i = resource["entity"]["name"]
    if model_name_i == target_model_name:
        Index = index
        break

if Index is None:   
    payload = {
      "type": "online",
      "name": DEPLOYMENT_NAME,
      "description": "This model is from jenkins deployment",
      "author": {
        "name": "authorA",
        "email": "authorA@example.com"
      },
      "deploy_info": {
        "scoringGroupId": str(SCORING_ID),
        "engineType": "scikit",
        "artifactVersionHref":  str(ARTIFACT_VERSION),
        "zaiu": True,
        "batching": {
          "maxLatencyInMs": 10,
          "maxBatchSize": 8
        }
      }
    }
    print(payload)

    response = requests.post(MY_URL, json=payload, headers=header,verify=False)

    SCORING_URL = json.loads(response.text)['entity']['scoring_url']

    SCORING_URL_Dict = {"scoring_url": SCORING_URL}

    with open(str(WORKSPACE)+'/scoringURL.json', 'w') as f:
        json.dump(SCORING_URL_Dict,f)

else:
    print("Model with same version is already available in Watson machine learning for z/OS.")