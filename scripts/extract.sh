#!/bin/env python
import json
import requests
import sys

WORKSPACE = sys.argv[2]
with open(str(WORKSPACE)+'/token.json') as f:
    data = json.load(f)

token = data['token']
Username = data['username']

header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
            'ML-Instance-Id' : Username
}
 
target_model_name = sys.argv[1]

with open(str(WORKSPACE)+'/extracted_userData.json') as f:
    data = json.load(f)

MainframeIP = data['MainframeIP']
  
response = requests.get("https://"+ MainframeIP +":11442/v3/published_models", headers=header,verify=False)

data = json.loads(response.text)

for index, resource in enumerate(data["resources"]):
    model_name = resource["entity"]["name"]
    if model_name == target_model_name:
        Index = index
        break

artifact_version = (json.loads(response.text)['resources'][Index]['entity']['latest_version']['url'][27:])

deploy_id = (json.loads(response.text)['resources'][Index]['metadata']['guid'])

deployment_urls = (json.loads(response.text)['resources'][Index]['entity']['deployments']['url'])

model_version = (json.loads(response.text)['resources'][Index]['entity']['versionSeq'])

extracted_val = {"artifact_version": artifact_version, "model_url": deployment_urls, "model_id": deploy_id, "model_name": target_model_name, "model_version": model_version}

with open(str(WORKSPACE)+'/extracted_data.json', 'w') as f:
    json.dump(extracted_val, f)
