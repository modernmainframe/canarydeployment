import sys
import requests
import json

class SomethingWrong(Exception):
     pass

header = {
            'Content-Type': 'application/json',
            'Control': 'no-cache'
}

payload = {
 "username":sys.argv[2], "password": sys.argv[3]
}

MainframeIP = sys.argv[1]
extracted_val = {"username": payload["username"], "MainframeIP": MainframeIP}

WORKSPACE = sys.argv[4]
with open(str(WORKSPACE) +'/extracted_userData.json', 'w') as f:
    json.dump(extracted_val, f)

MY_URL = "https://" + MainframeIP + ":9888/auth/generateToken"

response = requests.post(MY_URL, json=payload, headers=header,verify=False)

if(json.loads(response.text)["_messageCode_"] != "success"):
    print(json.loads(response.text)["_messageCode_"])
    raise(SomethingWrong("please check your password"))
else:
    print(response.text)
