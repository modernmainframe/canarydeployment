#!/bin/env sh
${WORKSPACE}/scripts/extract.sh $1 ${WORKSPACE}

ARTIFACT_VERSION=$(python ${WORKSPACE}/scripts/json_extract.py artifact_version ${WORKSPACE})
MODEL_URL=$(python ${WORKSPACE}/scripts/json_extract.py model_url ${WORKSPACE})
MODEL_VERSION=$(python ${WORKSPACE}/scripts/json_extract.py model_version ${WORKSPACE})
SCORING_ID=$(python ${WORKSPACE}/scripts/extractScoringID.py $2 ${WORKSPACE})

echo "ARTIFACT_VERSION: $ARTIFACT_VERSION"
echo "MODEL_URL       : $MODEL_URL"
echo "MODEL_VERSION   : $MODEL_VERSION"
echo "SCORING_ID      : $SCORING_ID"

python ${WORKSPACE}/scripts/deploy.py $ARTIFACT_VERSION $MODEL_URL $MODEL_VERSION ${WORKSPACE} $SCORING_ID
