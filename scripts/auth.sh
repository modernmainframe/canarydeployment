#!/bin/env sh

# auth_helper.py has mainframe IP, wmlz user and password as argument to generate token for authentication.
python ${WORKSPACE}/scripts/auth_helper.py $1 $2 $3 ${WORKSPACE} > ${WORKSPACE}/token.json
