#!/bin/env python
import json
import requests
import sys

WORKSPACE = sys.argv[2]
with open(str(WORKSPACE)+'/token.json') as f:
    data = json.load(f)

token = data['token']

with open(str(WORKSPACE)+'/scoringURL.json') as f:
    data = json.load(f)

SCORING_URL = data["scoring_url"]

print(SCORING_URL)
header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
            'ML-Instance-Id' : 'ibmuser'
}

filename = WORKSPACE + '/' + sys.argv[1]
with open(filename) as f:
    data = json.load(f)

payload = data['payload']
response = requests.post(SCORING_URL, json=payload, headers=header,verify=False)

print(json.loads(response.text))

