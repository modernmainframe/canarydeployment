import json
import requests
import sys

WORKSPACE = sys.argv[2]
with open(str(WORKSPACE)+'/token.json') as f:
    data = json.load(f)

token = data['token']

with open(str(WORKSPACE)+'/extracted_userData.json') as f:
    data = json.load(f)

MainframeIP = data['MainframeIP']
Username = data['username']

header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
            'ML-Instance-Id' : Username
}
  
response = requests.get("https://"+ MainframeIP +":11442/v3/deployments/ha/scoring/cluster", headers=header,verify=False)

data = json.loads(response.text)

target_scoring_name = sys.argv[1]

Index = None

for index, resource in enumerate(data["resources"]):
    scoring_id_i = resource["entity"]["scoringGroupName"]
    if scoring_id_i == target_scoring_name:
        Index = index
        break

scoring_id = (json.loads(response.text)['resources'][Index]['metadata']['guid'])

print(scoring_id)